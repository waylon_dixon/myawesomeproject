<?php

$description = 'Selling Your Home? Call for a free consultation. We are available days, nights and weekends.';
$title = 'Top Rated Agent in Las Vegas | Earl White, LLC';

  $keywords = '';

  include('includeHead.php');
  
?>
  <body id="thank-you">
  <?php include('includeHeader.php'); ?>
  <main role="main">
    <!-- Hero Row -->
    <header class="hero-row" id="get-started">
      <div class="container">
        <div class="row d-flex align-items-center my-5">
          <div class="col-12">
            <h1 class="font-weight-bold text-white text-center">Thank You</h1>
          </div>
        </div>
      </div>
    </header>

    <!-- Choose Your Plan Row -->
    <div class="container">
      <div class="row">
        <div class="col-lg-4 d-none d-lg-block text-center">
            <div class="thank-you-image"></div>
        </div>
        <div class="col-lg-8 col-sm-12">
            <h2>Thank you for contacting us</h2>
            <p>We will contact you to book your free consultation. Or if you need help immediately call <?=$phoneNumber?>.</p>
        </div>
      </div>

    </div>
    <!-- /.container -->
  </main>

    <?php include('includeFooter.php'); ?>
  </body>

</html>
