<?php

  $description = '';
  $title = 'Expert Internet Marketing - Page Name';
  $keywords = '';
  $businessName = 'Fitchman & Associates';

  include('includeHead.php');

  //** Hero Variables */
  $heroTitle = 'Discover Your Company\'s True Value' ;
  $heroSubtitle = 'Schedule your free consultation to learn how to get the most out of your business';
  $formTitle = 'Schedule your free consultation today';
  $formSubtitle = 'Complete the form below <br />or call '.$phoneNumber . 'to get started with our service and products.';

  //** Benefits Variables */
  $benefitsTitle = 'Services Customized For Your Specific Business';
  $benefitsSubTitle = $businessName.' unique approach can help you determine your true business value, evaluate your existing processes, and provide you with the financial management tools you need to increase the value of your business, plan your exit strategy, and sell your business when you are ready. Our customized services include:';

  $benefit1Title = 'Business Valuation';
  $benefit1Alt = 'Business Valuation';
  $benefit1SubTitle = 'Determine what your business is worth.';

  $benefit2Title = 'Value Management';
  $benefit2Alt = 'Value Management';
  $benefit2SubTitle = 'Manage and grow the value of your business.';

  $benefit3Title = 'Value Builders';
  $benefit3Alt = 'Value Builders';
  $benefit3SubTitle = 'Work in a group with other qualified business owners.';

  $benefit4Title = 'Value Achievement';
  $benefit4Alt = 'Value Achievement';
  $benefit4SubTitle = 'Realize maximum value upon sale of your business.';

  $aboutTitle = '40 Plus Years of Financial Industry Experience';
  $aboutBullets = [
      "Banking investment and federal bank regulatory experience.",
      "MBA, Adjunct professor at UNLV in finance, commercial banking, and management and leadership.",
      "Program coordinator at Lee Business School Commercial Banking.",
      "Instructor of U.S. Small Business Administration's Emerging Leaders program.",
      "Nevada Real estate license and business broker permit.",
      "Las Vegas business owner for 18 years.",
  ];
  
?>
  <body id="home">
  <?php include('includeHeader.php'); ?>
  <main role="main">

    <!-- Hero Row -->
    <header class="hero-row" id="get-started">
      <div class="image-container">

      </div>
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-lg-8">
            <h1 class="font-weight-bold text-white"><?=$heroTitle?></h1>
            <h2 class="text-yellow subhero text-uppercase" style="font-weight: bolder;"><?=$heroSubtitle?></h2>
          </div>
          <div class="col-lg-4" id="get-started-form">
            <div class="bg-primary-color p-4 text-white">
              <h3 class="text-center font-weight-bold"><?=$formTitle?></h3>
              <p class="text-center text-white"><?=$formSubtitle?></p>
              <form method="post" name="contactform" id="contactform" class="" novalidate="novalidate" action="">
                <div class="form-group">
                  <label class="text-uppercase contact-form-label">Full Name</label>
                  <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                  <label class="text-uppercase contact-form-label">Email address</label>
                  <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div  class="form-group">
                  <label class="text-uppercase contact-form-label">Phone Number</label>
                  <input type="text" class="form-control" id="phone" name="phone" required>
                </div>
                <div class="form-group">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="terms" required>
                    <label class="form-check-label text-small" for="terms">
                      By checking this box you are agreeing to our
                      website <a target="_blank" href="/privacy-policy">Privacy Policy</a> and <a target="_blank" href="/terms">Terms of Use</a>.
                    </label>
                  </div>
                </div>
                <button class="btn text-uppercase btn-primary hover-fade btn-lg btn-block"><?=$ctaButtonLabel?></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </header>
      <!-- Benefits Row -->
      <div class="bg-gray" id="benefits">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h2 class="text-secondary-color text-center"><?=$benefitsTitle?></h2>
                      <p class="row-subtitle"><?=$benefitsSubTitle?></p>
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/icon-business-valuation.png" alt="<?=$benefit1Alt?>"/>
                      <h4 class="text-accent-color"><?=$benefit1Title?></h4>
                      <p><?=$benefit1SubTitle?></p>
                  </div>
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/icon-value-management.png" alt="<?=$benefit2Alt?>"/>
                      <h4 class="text-accent-color"><?=$benefit2Title?></h4>
                      <p><?=$benefit2SubTitle?></p>
                  </div>
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/icon-value-builders.png" alt="<?=$benefit3Alt?>"/>
                      <h4 class="text-accent-color"><?=$benefit3Title?></h4>
                      <p><?=$benefit3SubTitle?></p>
                  </div>
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/icon-value-achievement.png" alt="<?=$benefit4Alt?>"/>
                      <h4 class="text-accent-color"><?=$benefit4Title?></h4>
                      <p><?=$benefit4SubTitle?></p>
                  </div>
              </div>

          </div>
      </div>


            <!-- CTA Row - Small -->
      <div class="bg-primary-color text-center text-white ready-to-start" id="cta-1">
          <div class="container">
              <div class="row" style="align-items: center;">
                  <div class="col-lg-8">
                      <h2 class="mb-lg-0">Call <?=$phoneNumber?> to Get Started Now</h2>
                  </div>
                  <div class="col-lg-4">
                      <a class="btn btn-primary text-uppercase hover-fade btn-lg" href="/#get-started-form" role="button"><?=$ctaButtonLabel?></a>
                  </div>
              </div>
          </div>
      </div>
    
     <!-- About Row -->
    <div id="about">
      <div class="container">
        <div class="row mb-2">
            <div class="col-lg-6 text-white">
              &nbsp;
            </div>

            <div class="col-lg-6 text-white" style="padding: 2rem 1.5rem; background: rgba(0,0,0,0.5); position: relative; right:0px;">
                <h2><?=$aboutTitle?></h2>
                <p style="line-height: 1.75rem;">
                <ul>
                <?php
                  foreach($aboutBullets as $bullet) {
                    printf('
                      <li>%s</li>
                    ',
                    $bullet
                  );
                  }
                ?>
                </ul>
            </div>
        </div>
      </div>
    </div>

      <!-- A Row -->
      <div id="credentials">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h2 class="text-secondary-color text-center">Our Affiliations</h2>
                      <p class="row-subtitle"><?=$businessName?> is proudly affiliated with the following organizations and institutions, which speaks to our commitment, our experience, and our ability to deliver.</p>
                  </div>
              </div>
              <div class="row credential-icons">
                  <div class="col-lg text-center">
                      <div class="cred-images">
                          <img alt="Greater Vegas Association of Realtors" src="/assets/logo-glvr.jpg">
                          <img alt="SBA" src="/assets/logo-sba.jpg">
                          <img alt="UNLV Lee Business School" src="/assets/logo-unlv.jpg">
                          <img alt="Emerging Leaders" src="/assets/logo-el.jpg">
                          <img alt="Century 21 Americana" src="/assets/logo-c21.jpg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
    
      <div id="testimonials" style="background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), url(/assets/background-testimonial.jpg); background-repeat: no-repeat;background-attachment: fixed;background-size: cover;">
          <div class="container text-white">
              <div class="row">
                  <div class="col-12">
                      <h2 class="text-center">Customer Testimonials</h2>
                      <p class="row-subtitle" style="font-size: .75rem; margin-bottom: 50px;">Real customers with real results. Learn about the benefits of working with <?=$businessName?>.</p>
                  </div>
              </div>

              <div id="carouselContent" class="carousel slide" data-ride="carousel">

                  <div class="carousel-inner active" role="listbox">
                      <div class="carousel-item active text-center p-4">
                          <span class="d-block mb-5">&ldquo;...helped me break down the numbers and information so I could understand our path to growth.&rdquo;</span>
                          <h3 class="">Mike H.</h3>
                      </div>
                      <div class="carousel-item text-center p-4">
                          <span class="d-block mb-5">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&rdquo;</span>
                          <h3 class="">01/31/2020<br>John Smith<br>ABC Company, Inc.</h3>
                      </div>
                      <div class="carousel-item text-center p-4">
                          <span class="d-block mb-5">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&rdquo;</span>
                          <h3 class="">01/07/2020<br>John Smith<br>ABC Company, Inc.</h3>
                      </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselContent" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                  </a>
                  <ol class="carousel-indicators" style="position: absolute;">
                      <li data-target="#carouselContent" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselContent" data-slide-to="1" class="active"></li>
                      <li data-target="#carouselContent" data-slide-to="2" class="active"></li>
                  </ol>
              </div>
          </div>
      </div>

      <div class="the-team">
            <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h2 class="text-secondary-color text-center">Meet Our Team</h2>
                      <p class="row-subtitle">
                        <?=$businessName?> team includes valuation experts with Big Four accounting firm experience who
                        hold CPA, ABV, CFA, ASA, and CDBV accreditations, as well as members of American Society of Appraisers,
                        Appraisal Issues Task Force, American Institute of Certified Public Accountants and Business Valuation
                        Association.
                      </p>
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/team-david.jpg" alt="team-david"/>
                      <div class="container">
                        <h4 class="text-accent-color">David J. Fitchman</h4>
                        <h5>Chief Executive Officer</h5>
                        <p>
                          David serves high-net-worth individuals in estate planning, financial transactions, and the
                          development of business tax strategies.
                        </p>
                      </div>
                  </div>
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/team-christina.jpg" alt="team-christina"/>
                      <div class="container">
                        <h4 class="text-accent-color">Christina Fithman</h4>
                        <h5>Partner</h5>
                        <p>
                          Christina focuses on firm-wide growth and bringing innovation to better create value
                          and drive successful outcomes for clients.
                        </p>
                      </div>
                  </div>
                  <div class="col-lg col-12 text-center">
                      <img class="icon" src="/assets/team-itay.jpg" alt="team-itay"/>
                      <div class="container">
                        <h4 class="text-accent-color">Itay Verchik</h4>
                        <h5>Partner</h5>
                        <p>
                          Itay specializes in providing valuation services and financial analysis to clients of all sizes
                          and stages of development for diverse purposes.
                        </p>
                      </div>
                  </div>
              </div>

          </div>
      </div>

    <!-- Ready to Start -->
    <div class="bg-primary-color text-center text-white ready-to-start" id="cta-2">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2>Are You Ready to Increase the Value of Your Business?</h2>
            <p class="row-subtitle">Let <?=$businessName?> help you grow your profits and realize the maximum value of your business.</p>
            <p class="mb-2">
            <a class="btn btn-primary text-uppercase hover-fade btn-lg" href="/#get-started-form" role="button"><?=$ctaButtonLabel?></a>
            <p class="large-text">Or Call: <?=$phoneNumber?></p>
            </p>
          </div>
        </div>
      </div>
    </div>
  </main>
  <?php include('includeFooter.php'); ?>
  </body>
</html>
