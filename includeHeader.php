<?php include('includeGtmNoScript.php'); ?>

<!-- Header Row -->
<div id="top" class="header bg-primary-color text-white">
    <div class="container-fluid container py-2">
        <div class="row">
            <div class="col-md-12 col-lg-6 text-xs-center">
                <span class="slogan"><em><?=$slogan?></em></span>
            </div>
            <div class="col-md-12 col-lg-6 text-right text-xs-center">
                <span class="hidden-sm hidden-xs"><?=$callLine?> </span>
                <span class="phone"><spam style="font-weight: bold; color:white !important;"><?=$phoneNumber?></span></span>
            </div>
        </div>
    </div>
</div>

<!-- Navbar Row -->
<div class="bg-light">
<nav class="navbar navbar-expand-xl navbar-light bg-light fixed-top"  data-toggle="affix">
    <div class="container-fluid container py-3">
        <a class="navbar-brand" href="/"><img src="/assets/logo-fitchman-associates.svg" alt="Fitchman Associates Logo" width="400" height="58"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto font-weight-bold text-uppercase">
                <li class="nav-item">
                    <a class="nav-link" href="/#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#services">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#testimonials">Testimonials</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#get-started-form">Get Started</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>