<?php include('includeFooterScripts.php'); ?>

<!-- Footer -->
    <footer class="py-0 bg-secondary-color">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center text-lg-left text-gray">
            <span class="d-block"><a href="/privacy-policy">Privacy Policy</a> | <a href="/terms">Terms of Use</a></span>
            <span class="d-block">Copyright &copy; <?php echo date("Y"); ?> <?=$businessName?>. All Rights Reserved.</span>
          </div>
          <div class="col-lg-6 text-gray text-center text-lg-right mt-3 mt-lg-0">
            <span class="d-block"><a href="http://expertinternetmarketing.com" target="_blank">Website Design</a> &amp; <a href="http://expertinternetmarketing.com" target="_blank">Online Advertising</a> provided by <br><a href="http://expertinternetmarketing.com" target="_blank">Expert Internet Marketing</a></span>
          </div>
        </div>
      </div>
      <!-- /.container -->
    </footer>